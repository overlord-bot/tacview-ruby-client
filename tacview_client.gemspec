# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'tacview_client/version'

Gem::Specification.new do |spec|
  spec.name          = 'tacview_client'
  spec.version       = TacviewClient::VERSION
  spec.authors       = ['Jeffrey Jones']
  spec.email         = ['jeff@jones.be']

  spec.summary       = 'Tacview client written in ruby'
  spec.description   = 'Tacview client written in ruby'
  spec.homepage      = 'https://gitlab.com/overlord-bot/tacview-ruby-client'
  spec.license       = 'MIT'

  spec.required_ruby_version = '>=2.5'

  spec.files = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.metadata['yard.run'] = 'yri'

  spec.add_dependency 'crc', '~> 0.4'

  spec.add_development_dependency 'bundler', '~> 2.0'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.8'
  spec.add_development_dependency 'rubocop', '~>0.73'
  spec.add_development_dependency 'simplecov', '~>0.17'
  spec.add_development_dependency 'yard', '~>0.9'
end
